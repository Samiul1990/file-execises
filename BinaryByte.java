import java.io.*;

public class BinaryByte{
	public static void main(String[] args) throws Exception{
			File f1 = new File(args[0]);
			File f2 = new File(args[1]);
			
			FileInputStream fis = new FileInputStream(f1);
			DataInputStream dis = new DataInputStream(fis);
			
			FileOutputStream fos = new FileOutputStream(f2);
			DataOutputStream dos = new DataOutputStream(fos);
			
			int availableBytes = 0;
			while((availableBytes = fis.read()) != 0){
				fos.write(availableBytes);
			}
			dos.flush();
			dos.close();		
			fos.close();
			
			dis.close();
			fis.close();
			
		
		
		
	}
}