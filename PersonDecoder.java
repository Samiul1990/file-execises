import java.io.*;
import java.util.*;

public class PersonDecoder{
	public static void main(String[] args)throws Exception{
		ArrayList<Person> personList = new ArrayList<>();
        try{
            BufferedReader bufferedReader = new BufferedReader(new FileReader("personList.txt"));
            String line;
            while ((line = bufferedReader.readLine()) != null){
                String[] fields = line.split(",");
                long id = Long.parseLong(fields[0]);
                String name = fields[1];
                int age = Integer.parseInt(fields[2]);
                Person p = new Person(id, name, age);
				personList.add(p);	
            }
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }finally {
            for(Person p : personList){
			System.out.println(p.toString());
		}
        }
	}
}