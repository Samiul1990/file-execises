import java.io.*;

class CopyFile{
	public static void main(String[] args) throws Exception{
		FileInputStream fis = new FileInputStream("Lotus.jpg");
		DataInputStream dis = new DataInputStream(fis);
		
		FileOutputStream fos = new FileOutputStream("Lotus.jpg");
		DataOutputStream dos = new DataOutputStream(fos);
		
		int availableBytes = 0;
		while((availableBytes = dis.available()) != 0){
			byte[] byteToRead = new byte[availableBytes];
			dis.read(byteToRead);
			
			for(int i = 0; i < byteToRead.length; i++){
				dos.write(byteToRead[i]);
				System.out.println(byteToRead[i]);
			}
		}
		dos.flush();
		dos.close();		
		fos.close();
		
		dis.close();
		fis.close();
	}
}
