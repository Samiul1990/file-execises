import java.io.*;
import java.util.*;

public class PersonEncoder{
	public static void main(String[] args)throws Exception{
		
		ArrayList<Person> personList = new ArrayList<>();
		
		Person p1 = new Person(1, "abc", 20);
		Person p2 = new Person(2, "def", 25);
		Person p3 = new Person(3, "ght", 30);
		
		personList.add(p1);
		personList.add(p2);
		personList.add(p3);
		
		FileWriter fw = new FileWriter("personList.txt");
		for(Person p : personList){
			String output = p.getId() + ","+p.getName()+","+p.getAge();
			fw.write(output + System.lineSeparator());
		}
		fw.close();
	}
	
}